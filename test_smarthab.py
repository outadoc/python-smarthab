#!/usr/bin/env python3

# pysmarthab - control devices in a SmartHab-powered home
# Copyright (C) 2019  Baptiste Candellier

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
import pysmarthab as smarthab


class LoginTests(unittest.TestCase):
    def setUp(self):
        self.smarthab = smarthab.SmartHab()

    def test_successful_login(self):
        self.smarthab._network = MockedNetwork('example.com')
        self.smarthab.login('xxxxxxx.xxxxxxx@xxxxxx.com', 'xxxxx')
        self.assertTrue(self.smarthab.is_logged_in())

    def test_failed_login(self):
        self.smarthab.login('nopenope@notreal.gov', '123456')
        self.assertFalse(self.smarthab.is_logged_in())


class HashTests(unittest.TestCase):
    def setUp(self):
        self.security = smarthab.Security()

    def test_simple_hash(self):
        key = 'secret'
        params = {'param': 3434}
        res = self.security.hmac_list(key, params)

        self.assertEqual(
            'beb7d3e2d00d3791247321dad73b5bd32208fd1da361af419eab339247e54e70', res)

    def test_multi_hash(self):
        key = 'secreter'
        params = {'param1': 234245, 'param2': 'test@email.co'}
        res = self.security.hmac_list(key, params)

        self.assertEqual(
            '1ae7d9b4582d5a98faa0d9998b98c01c5bde8b68e6fed74afa5eb0ee497240c5', res)

    def test_param_sort(self):
        key = 'secretish'
        params = {'param2': 'random', 'param3': 23452345, 'param1': 'lastone'}
        res = self.security.hmac_list(key, params)

        self.assertEqual(
            'e64175ba1af39036fca4d07540b52a54ad6b0639ae32ce6db03a1b05e50c7a21', res)

    def test_param_join(self):
        params = {'param2': 'random', 'param3': 23452345, 'param1': 'last@one'}
        res = self.security.params_to_query_string(params)

        self.assertEqual('param1=last@one&param2=random&param3=23452345', res)

    def test_add_to_params(self):
        key = 'secretish'
        params = {'param2': 'random', 'param3': 23452345, 'param1': 'lastone'}
        res = self.security.sign_params(key, params)

        self.assertEqual({
            'param1': 'lastone',
            'param2': 'random',
            'param3': 23452345,
            'sha': 'e64175ba1af39036fca4d07540b52a54ad6b0639ae32ce6db03a1b05e50c7a21'
        }, res)


class DeviceTests(unittest.TestCase):
    def setUp(self):
        self.smarthab = smarthab.SmartHab(network=MockedNetwork('example.com'))
        self.smarthab.login('xxxxxxx.xxxxxxx@xxxxxx.com', 'xxxxx')

    def test_get_devices(self):
        devices = self.smarthab.get_device_list()
        self.assertEqual(6, len(devices))

    def test_get_device_state(self):
        devices = self.smarthab.get_device_list()

        for device in devices:
            if device.device_id == '1-400-484':
                state = device.state
                self.assertFalse(state)
                return

        # No matching device
        self.fail()

    def test_set_device_state(self):
        devices = self.smarthab.get_device_list()

        for device in devices:
            if device.device_id == '1-400-484':
                self.assertFalse(device.state)
                device.state = True
                self.assertTrue(device.state)
                return

        # No matching device
        self.fail()


class ThermostatTests(unittest.TestCase):
    def setUp(self):
        self.smarthab = smarthab.SmartHab()
        self.smarthab.login('outadoc@gmail.com', '626203')

    def test_get_initial_thermostat_state(self):
        devices = self.smarthab.get_device_list()

        for device in devices:
            # Thermostat
            if device.device_id == '5-400-0':
                self.assertEqual(18.5, device.setpointM)
                self.assertEqual(21, device.setpoint0)
                self.assertEqual(19, device.setpoint1)
                self.assertEqual(16, device.setpointA)
                self.assertEqual(7, device.tempMin)
                self.assertEqual(30, device.tempMax)

                return

        # No matching device
        self.fail()

    def test_get_updated_thermostat_state(self):
        devices = self.smarthab.get_device_list()

        for device in devices:
            # Thermostat
            if device.device_id == '5-400-0':
                device.update_settings()

                self.assertEqual(18.5, device.setpointM)
                self.assertEqual(21, device.setpoint0)
                self.assertEqual(19, device.setpoint1)
                self.assertEqual(16, device.setpointA)
                self.assertEqual(7, device.tempMin)
                self.assertEqual(30, device.tempMax)

                return

        # No matching device
        self.fail()


class NetworkTests(unittest.TestCase):
    def test_default_url(self):
        hub = smarthab.SmartHab()
        self.assertEqual('https://api.smarthab.tech', hub._network.base_url)

    def test_custom_url(self):
        hub = smarthab.SmartHab(base_url='https://smarthab.example.com')
        self.assertEqual('https://smarthab.example.com', hub._network.base_url)


class BrokenDeviceTests(unittest.TestCase):
    def setUp(self):
        self.smarthab = smarthab.SmartHab(
            network=BrokenLight_MockedNetwork('example.com'))
        self.smarthab.login('xxxxxxx.xxxxxxx@xxxxxx.com', 'xxxxx')

    def test_set_broken_device_state(self):
        devices = self.smarthab.get_device_list()

        for device in devices:
            if device.device_id == 'zwave_device_deadbeef_node7_switch_binary':
                # Broken device state is "0" and should be interpreted as false
                self.assertFalse(device.state)

                # State can't be changed and can return an empty string
                device.state = True

                self.assertFalse(device.state)


class ExpiredTokenTests(unittest.TestCase):
    def setUp(self):
        good_network = MockedNetwork('example.com')
        bad_network = InvalidToken_MockedNetwork('example.com')
        bad_network.delegate = good_network
        good_network.state = bad_network.state

        self.smarthab = smarthab.SmartHab(network=bad_network)
        self.smarthab._state.token = 'blablabla'

    def test_renew_expired_token(self):
        devices = self.smarthab.get_device_list()
        self.assertNotEqual(None, self.smarthab._state.token)
        self.assertEqual(6, len(devices))


class MockedNetwork(smarthab.Network):
    async def async_send_request(self, endpoint, params=None, key=None, verb='GET'):
        file = None
        if endpoint == smarthab.Endpoint.GET_TOKEN:
            file = 'mock_getToken.json'

        if endpoint == smarthab.Endpoint.GET_OBJECTS:
            file = 'mock_getObjects.json'

        if endpoint == smarthab.Endpoint.GET_OBJECT_STATE:
            file = 'mock_getStateObject.json'

        if endpoint == smarthab.Endpoint.SET_OBJECT_STATE:
            file = 'mock_setObject.json'

        if file is None:
            raise NotImplementedError()

        with open("mock/{file}".format(file=file)) as f:
            return smarthab.Network._parse_response(f.read())


class BrokenLight_MockedNetwork(smarthab.Network):
    async def async_send_request(self, endpoint, params=None, key=None, verb='GET'):
        file = None
        if endpoint == smarthab.Endpoint.GET_TOKEN:
            file = 'mock_getToken.json'

        if endpoint == smarthab.Endpoint.GET_OBJECTS:
            file = 'mock_getObjects.json'

        if endpoint == smarthab.Endpoint.GET_OBJECT_STATE:
            file = 'mock_getStateObject_broken.json'

        if endpoint == smarthab.Endpoint.SET_OBJECT_STATE:
            file = 'mock_setObject_broken.json'

        if file is None:
            raise NotImplementedError()

        with open("mock/{file}".format(file=file)) as f:
            return smarthab.Network._parse_response(f.read())


class InvalidToken_MockedNetwork(smarthab.Network):
    done_once = False
    delegate = None

    async def async_send_request(self, endpoint, params=None, key=None, verb='GET'):
        if not self.done_once:
            self.done_once = True
            return smarthab.Network._parse_response('{"success": 0,"code": 11}')

        return await self.delegate.async_send_request(endpoint, params, key, verb)
